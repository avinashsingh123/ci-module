<?php 
class Login_Model extends CI_Model {

        public $title;
        public $content;
        public $date;
        public  $conn_new;
		public function __construct(){
				
		}
        public function login()
        {

			$this->db->select('*');
			$this->db->where(array('email'=>$this->input->post('email'),'password'=>$this->hash($this->input->post('password')),'status'=>'Active'));
			$query = $this->db->get('master');			
			if($query->num_rows()){
				$this->session->unset_userdata('s_admin');
				$row = $query->row();
								 		 
					$data = array(
                    'userid' => $row->id,
                    'name' => $row->name,
                    'email' => $row->email,
                    'phone' => $row->phone,
                    'address' => $row->address,
                    'isloddedIn' => true,
                    
                    );
	                 $this->session->set_userdata('admin',$data);
		           
		            return true;
		          
			}
			return false;
        }
       
		
        public function logout()
        {
		return $this->session->sess_destroy();
        }
		
		public function isloggedIn()
        {
		return (bool) $this->session->userdata('admin','isloddedIn');
        }
        public function hash($str)
        {
          return hash('md5',$str);    
        }
		


	}

?>