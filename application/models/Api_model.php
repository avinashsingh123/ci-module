<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model{

	 public function insert($data, $table, $id=null)
        {
			$this->db->set($data);
            if($id){
				//update data here
			$this->db->where('id',$id);
			return $this->db->update($table,$data);
			}else{
				//insert new record
			$this->db->insert($table,$data);	
			return $this->db->insert_id(); 
			}
        }
		
		
        public function secure_login($data,$table)
        {
		//	echo $this->input->post('username');
			$this->db->select('*');
			$this->db->where($data);
			$query = $this->db->get($table);
			if($query->num_rows()){
			$row = $query->row();
			unset($row->password);
            $this->session->set_userdata('admin',$data);
            return $row;
			}
		return false;
        }
		
		
        public function get($table, $id=null,$count=null)
        {
			 
			$this->db->select('*');
            if($id){
				//specific record
			$this->db->where('id',$id);
			$method = 'row';
			}else{
				//all record
			$method = 'result'; 
			}
			$method = $count ? 'num_rows' : $method;
			return $this->db->get($table)->$method();
        }
		
        public function get_where($table,$where,$id=null,$count=null)
        {  
			$this->db->where($where);
			
			return $this->get($table, $id, $count);
        }
		
        public function delete($table, $id)
        {
			if($id){
				//delete specific record
			$this->db->where('id',$id);
			return $this->db->delete($table);
			}
		return false; 
        }
		
		
		
        public function getapidata($data=array())
        {
			$post =array();
			 if(count($data)){
			 foreach($data as $key=>$val){
			 $post[$key] = $val;
			 }
			 }
		  return $post;
        }
 	 	
        public function get_new($data=array())
        {
			$new = new stdClass();
			 if(count($data)){
			 foreach($data as $key){
				 
			 $new->$key ='';
			 }
			 }
		  return $new;
        }
		
        public function check_duplicacy($table,$column,$colval)
        {
			 $this->db->select($column);
			 $this->db->where($column,$colval);
			 $query = $this->db->get($table);
			 return $query->num_rows() ? TRUE : FALSE;
        }				
		
		
		public function login($email, $password)
			{
		 
			$data = array();
			$this->db->select('*');
			$this->db->where(array(
				'email' => $email,
				'password' => $this->hash($password)
			));
			$query = $this->db->get('master');
			if ($query->num_rows())
				{
				$row = $query->row();
				$data = array(
					'userid' => $row->id,
					'username' => $row->username,
					'email' => $row->email,
					'isloddedIn' => true
				);
				$this->session->set_userdata('admin', $data);
				return $data;
				}

			return $data;
			}

			public function hash($str)
				{
				return hash('md5', $str);
				}
		function send_email($email_address = '', $pwd = '')
		{
			 
			  $this->email->from('uppertechsolutions.in', 'NO REPLY');
			  $this->email->to($email_address);

			  $this->email->subject('Password Changed Successfully');
			  $this->email->message('Your password has been changed successfully'.$pwd);
			   if ( ! $this->email->send())
			   {
				  //echo "mail error";
			   }else{
				  // echo "successfully";
			   }
			  $this->email->send();
					
		}
		function send_email_user($email_address ,$msg='')
		{
			 
			  $this->email->from('avinash@nextolive.com', 'NO REPLY');
			  $this->email->to($email_address);

			  $this->email->subject('Account Activated Successfully');
			  $this->email->message($msg);
			   if ( ! $this->email->send())
			   {
				  //echo "mail error";
			   }else{
				  // echo "successfully";
			   }
			 // $this->email->send();
					
		}
		function db_fields_check($table='',$fieldname=''){
			$fields = $this->db->field_data($table);
				$fl = 0;
				foreach ($fields as $field)
				{
				   if($fieldname == $field->name){
					   $fl = 1;
				   }
				}
				if($fl)
					return true;
				else
					return false;

		}

}