<?php 
class Master_Model extends CI_Model {

        public $title;
        public $content;
        public $date;
		

        public function get_last_ten_entries()
        {
                $query = $this->db->get('entries', 10);
                return $query->result();
        }

        public function insert_entry()
        {
                $this->title    = $_POST['title']; // please read the below note
                $this->content  = $_POST['content'];
                $this->date     = time();

                $this->db->insert('entries', $this);
        }

        public function update_entry()
        {
                $this->title    = $_POST['title'];
                $this->content  = $_POST['content'];
                $this->date     = time();

                $this->db->update('entries', $this, array('id' => $_POST['id']));
        }
		
		
        public function login()
        {
		//	echo $this->input->post('username');
			$this->db->select('*');
			$this->db->where(array('username'=>$this->input->post('username'),'password'=>$this->hash($this->input->post('password'))));
			$query = $this->db->get('master');
			if($query->num_rows()){
				 $row = $query->row();
					$data = array(
                    'userid' => $row->id,
                  
                    'username' => $row->username,
                
                    'email' => $row->email,
                  
                    'isloddedIn' => true
                    );
            $this->session->set_userdata('admin',$data);
            return true;
			}
		return false;
        }
		
		
        public function logout()
        {
		return $this->session->sess_destroy();
        }
		
		
		
        public function isloggedIn()
        {
		return (bool) $this->session->userdata('admin','isloddedIn');
        }
		
		
		
        public function hash($str)
        {
          return hash('md5',$str);    
        }
		
        public function insert($data, $table, $id=null)
        {
            if($id){
				$this->db->where('id',$id);
				return $this->db->update($table,$data);
			}else{
				$this->db->insert($table,$data);	
				return $this->db->insert_id(); 
			}
			//exit;
        }
		
        public function get($table, $id=null,$count=null)
        {
			 
			$this->db->select('*');
            if($id){
				$this->db->where('id',$id);
			$method = 'row';
			}else{
				$method = 'result'; 
			}
			$method = $count ? 'num_rows' : $method;
			return $this->db->get($table)->$method();
        }
		
        public function get_where($table,$where,$id=null,$count=null)
        {  
			$this->db->where($where);
			
			return $this->get($table, $id, $count);
        }
		
        public function delete($table, $id)
        {
			if($id){
				$this->db->where('id',$id);
				return $this->db->delete($table);
			}
		return false; 
        }
		
		
		
        public function getposteddata($data=array())
        {
			$post =[];
			 if(count($data)){
				 foreach($data as $key){
				 $post[$key] = $this->input->post($key);
				 }
			 }
		  return $post;
        }
 	 	
        public function get_new($data=array())
        {
			$new = new stdClass();
			 if(count($data)){
				 foreach($data as $key){					 
					 $new->$key ='';
				 }
			 }
		  return $new;
        }
		
		public function get_current_user_id(){
			$check_login = $this->session->userdata('admin');
			if(!empty($check_login)){
				return  $check_login['userid'];
			}
			return ;
		}
		
		public function get_subcategory($id=''){
		
		$this->db->select('category.id as category_id,category.name as category_name,subcategory.*')
         ->from('subcategory')
         ->join('category', 'subcategory.category_id = category.id','left');
         if ($id) {
         	$this->db->where('subcategory.id',$id);
         }
         $this->db->where('subcategory.status !=','Delete');
		 $this->db->order_by('category.id','ASC');

		$result = $this->db->get();
		 $method = $id ? 'row':'result';
		return $result->num_rows() ? $result->$method() : array();

		}


		public function get_workouts($id=''){
		
		$this->db->select('category.name as category_name,subcategory.name as subcategory_name,workouts.*')
         ->from('workouts')         
         ->join('category', 'workouts.category_id = category.id','left')
         ->join('subcategory', 'subcategory.id= workouts.subcategory_id','left');
         if ($id) {
         	$this->db->where('workouts.id',$id);
         }
		 $this->db->where('workouts.status !=','Delete');
		 $this->db->order_by('workouts.id','DESC');
		$result = $this->db->get();
		 $method = $id ? 'row':'result';
		return $result->num_rows() ? $result->$method() : array();

		}

		public function get_feedback($id=''){
		
		$this->db->select('feedback.*,workouts.name as workout_name,master.name')
         ->from('feedback')         
         ->join('master', 'feedback.user_id = master.id','left')
         ->join('workouts', 'workouts.id= feedback.workout_id','left');
         if ($id) {
         	$this->db->where('feedback.id',$id);
         }
		 $this->db->where('feedback.status !=','Delete');
		 $this->db->order_by('feedback.id','DESC');
		$result = $this->db->get();
		 $method = $id ? 'row':'result';
		return $result->num_rows() ? $result->$method() : array();

		}
		
	
}
?>