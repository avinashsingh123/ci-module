<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offers extends CI_Controller {
	 public function __construct(){
		parent::__construct();
		$data = array();
		$this->load->model('master_model','master_m');
		$this->load->library('form_validation');
        if($this->master_m->isloggedIn() == FALSE ){ 
         	redirect('login/index');
        }
  	
	}
	public function index()
	{
		
			$data['title']='Manage offers';
		 	$data['offer'] = $this->master_m->get_where($table ='offers',['status !='=>'Delete']) ;
		    $this->load->view('elements/header',$data);
			$this->load->view('offers/manage_offers',$data);
			$this->load->view('elements/footer');
	}
	
	public function add_offers($id=null)
    {
		$data['title']='Add New Offer';
		$data['category']=$this->master_m->get('category');
		$data['subcategory']=$this->master_m->get('subcategory');

		
		if($id){
			$data['offers'] = $this->master_m->get('offers',$id) ;
			$data['title']='Edit Offer';
		}else{
			$data['offers'] = $this->master_m->get_new(array('id','amount','name','status','description')) ;	
		}
		
		if($this->input->server('REQUEST_METHOD')=='POST'){			
			$InsertArr = $this->master_m->getposteddata(array('name','amount','status','description'));		
			$InsertArr['onadd']= $id ? $data['offers']->onadd : date('Y-m-d H:i:s');
			if($this->master_m->insert($InsertArr, $table='offers', $id)){
				redirect('offers/index');
			}
		} 
		
		$this->load->view('elements/header',$data);
		$this->load->view('offers/add_offers',$data);
		$this->load->view('elements/footer');
    }
    public function delete_offers($id)
    {
		if($id){
			if($this->master_m->insert(['status'=>'Delete'],'offers',$id)){
				redirect('offers/index');		
			}	 
		}
    }
	
	
}