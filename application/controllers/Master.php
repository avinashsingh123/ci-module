<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master extends CI_Controller {
	 public function __construct(){
		parent::__construct();
		$data = array();
		$this->load->model('master_model','master_m');
		$this->load->library('form_validation');
        if($this->master_m->isloggedIn() == FALSE ){ 
         	redirect('login/index');
        }
  	
	}
	public function index()
	{
		
			$data['title']='Dashboard';
			$this->load->view('elements/header',$data);
			$this->load->view('master/dashboard',$data);
			$this->load->view('elements/footer');
	}
	
	public function feedback()
	{
			$data['title']='Feedback';
			$data['feedback']=$this->master_m->get_feedback();
			$this->load->view('elements/header',$data);
			$this->load->view('master/feedback',$data);
			$this->load->view('elements/footer');
	}
	public function delete_feedback($id)
    {
		if($id){
			if($this->master_m->insert(['status'=>'Delete'],'feedback',$id)){
				redirect('master/feedback');		
			}	 
		}
    }
	
}