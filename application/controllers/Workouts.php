<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Workouts extends CI_Controller {
	 public function __construct(){
		parent::__construct();
		$data = array();
		$this->load->model('master_model','master_m');
		$this->load->library('form_validation');
        if($this->master_m->isloggedIn() == FALSE ){ 
         	redirect('login/index');
        }
  	
	}
	public function index()
		{
		  $data['title']='Manage Workouts';
	 	  $data['workouts'] = $this->master_m->get_workouts() ;
		 // $data['pathology'] = $this->master_m->get_pathology_report() ;
		    $this->load->view('elements/header',$data);
			$this->load->view('workouts/manage_workouts',$data);
			$this->load->view('elements/footer');
		}
	
	// Plans Workouts starts
	public function add_workouts($id=null)
    {
		$data['title']='Add New Workouts';
		$data['category']=$this->master_m->get('category');
		$data['subcategory']=$this->master_m->get('subcategory');

		
		if($id){
			$data['workouts'] = $this->master_m->get_workouts($id) ;
			$data['title']='Edit Workouts';
		}else{
			$data['workouts'] = $this->master_m->get_new(array('id','category_id','subcategory_id','name','status','description')) ;	
		}
		
		if($this->input->server('REQUEST_METHOD')=='POST'){			
			$InsertArr = $this->master_m->getposteddata(array('name','category_id','subcategory_id','status','description'));		
			$InsertArr['onadd']= $id ? $data['workouts']->onadd : date('Y-m-d H:i:s');
			if($this->master_m->insert($InsertArr, $table='workouts', $id)){
				redirect('workouts/index');
			}
		} 
		
		$this->load->view('elements/header',$data);
		$this->load->view('workouts/add_workouts',$data);
		$this->load->view('elements/footer');
    }
		public function delete_workouts($id)
	    {

			if($id){
				if($this->master_m->delete($table ='workouts',$id)){
					redirect('workouts/index');		
				}	 
			}
	    }

	    public function get_subcategory()
	    {
	    	if ($this->input->is_ajax_request()) {
	    		$id = $this->input->post('code');
	    		$ddd = $this->master_m->get_where('subcategory',['category_id'=>$id]);
	    		$json_ar = [];
	    		if (!empty($ddd)) {
	    			foreach ($ddd as $value) {
	    				$json_ar[$value->id] = $value->name ;
	    			}
	    		}
	    		echo json_encode($json_ar);
	    		die();
	    	}
	    }

    
	
	
}