<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class API extends CI_Controller{

	protected $status = 'failed';
	protected $errorcode = '101';
	protected $error = TRUE;
	protected $message = 'Invalid Message';
	
	
    public function __construct() {
	    parent::__construct();
	    $this->load->model('Api_model','api_m');
	    $this->load->library('CommonExp', '', 'common');
	}   
	protected function output($AP = array()){
	$opt = array(
		'error'=>$this->error,
		'status'=>$this->status,
		'errorcode'=>$this->errorcode,
		'message'=>$this->message
	);
	if(count($AP)){
	 	$opt = $opt+$AP;
	}
	echo json_encode($opt);
	}
	
	protected function api_validate($error,$status,$errorcode,$message){
		$this->error = $error;	
		$this->status = $status;	
		$this->errorcode = $errorcode;	
		$this->message = $message;		
	}
	
	protected function msg($key){
		return $this->common->exceptions()[$key];
	}
	
	
}