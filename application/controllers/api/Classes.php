<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Classes extends CI_Controller{

	protected $status = 'failed';
	protected $errorcode = '101';
	protected $error = TRUE;
	protected $message = 'Invalid Message';
	
	
    public function __construct() {
    parent::__construct();
    $this->load->model('Api_model','api_m');
    $this->load->library('CommonExp', '', 'common');
	}   
	protected function output($AP = array()){
	$opt = array(
	'error'=>$this->error,
	'status'=>$this->status,
	'errorcode'=>$this->errorcode,
	'message'=>$this->message
	);
	if(count($AP)){
	 $opt = $opt+$AP;
	}
	echo json_encode($opt);
	}
	
	protected function api_validate($error,$status,$errorcode,$message){
	$this->error = $error;	
	$this->status = $status;	
	$this->errorcode = $errorcode;	
	$this->message = $message;		
	}
	
	protected function msg($key){
		return $this->common->exceptions()[$key];
	}
	
	public function getclass()
	{	
	$data = array();
	$class = $this->api_m->get_where($table="class",array());	
	if(count($class)){	
	$data['class'] = $class;		
	$this->api_validate($error=FALSE,$status='success',$errorcode=102,'class Loaded');			
	}else{	
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'class Not Found');			
	}	
	 
	$this->output($data);			
	} 
	
	public function getclassById()
	{	
	$data = array();
	$postdata =(array) json_decode(file_get_contents('php://input'));
	if(!empty($postdata)) {
	if(isset($postdata['id']) && $postdata['id']==''){
		$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'Invalied Class id');		
	}else{	
	$class = $this->api_m->get_where($table="class",array('id'=>$postdata['id']));	
	if(count($class)){	
	$data['class'] = $class;		
	$this->api_validate($error=FALSE,$status='success',$errorcode=102,'Class Loaded');			
	}else{	
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'Class Not Found');			
	}
	}
	}	
	$this->output($data);	
	 		
	} 
}