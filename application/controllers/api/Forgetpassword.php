<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Forgetpassword extends CI_Controller{

	protected $status = 'failed';
	protected $statuscode = '101';
	protected $error = TRUE;
	protected $message = 'Invalid Message';
	
	
    public function __construct() {
    parent::__construct();
    $this->load->model('Api_model','api_m');
    $this->load->library('CommonExp', '', 'common');
	$this->load->library('email');
	}   
	protected function output($AP = array()){
	$opt = array(
	'error'=>$this->error,
	'status'=>$this->status,
	'statuscode'=>$this->statuscode,
	'message'=>$this->message
	);
	if(count($AP)){
	 $opt = $opt+$AP;
	}
	echo json_encode($opt);
	}
	
	protected function api_validate($error,$status,$statuscode,$message){
	$this->error = $error;	
	$this->status = $status;	
	$this->statuscode = $statuscode;	
	$this->message = $message;		
	}
	
	protected function msg($key){
		return $this->common->exceptions()[$key];
	}
	
	public function index(){
		
	$data = array();
	$postdata =(array) json_decode(file_get_contents('php://input'));
	
	if(!empty($postdata )) {
	
	if(isset($postdata['email']) && $postdata['email']==''){
	
	$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('email_required'));
	
	}else if(isset($postdata['usertype']) && $postdata['usertype']==''){
	
	$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('usertype_required'));

	}else{
		
	$email =trim($postdata['email']);
	$type = trim($postdata['usertype']);
	//'type' =>trim($postdata['type'])
	
	if($type =='STUDENT'){
		
			// student login start code
			$logindata = $this->api_m->get_where($table='student',array('email'=>$email));
			if(!empty($logindata)){
				//print_r($logindata);
				$id = $logindata['0']->id;
				$rnd =  mt_rand(100000, 999999);
				
				$data = array('password'=>$rnd);
				if($this->api_m->insert($data,'student',$id)){
					$this->api_m->send_email($email, $pwd = $rnd);
					$this->api_validate($error=FALSE,$status='success',$statuscode=102,$this->msg('pwd_changed'));	
				}else{
					$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('error_update'));
				}
			
			}else {
				
			$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('invalid_access'));	
			}
		// student login end code	
		}else if($type=='FACULTY'){
				// faculty login start code
			$logindata = $this->api_m->get_where($table='faculty',array('email'=>$email));
			if(!empty($logindata)){
				//print_r($logindata);
				$id = $logindata['0']->id;
				$rnd =  mt_rand(100000, 999999);
				
				$data = array('password'=>$rnd);
				if($this->api_m->insert($data,'faculty',$id)){
					$this->api_m->send_email($email, $pwd = $rnd);
					$this->api_validate($error=FALSE,$status='success',$statuscode=102,$this->msg('pwd_changed'));	
				}else{
					$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('error_update'));
				}
			
			}else {
				
			$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('invalid_access'));	
			}// faculty login end code
		}else if($type=='ADMIN'){
				// admin login start code
			$logindata = $this->api_m->get_where($table='master',array('email'=>$email));
			if(!empty($logindata)){
				
				$id = $logindata['0']->id;
				$rnd =  mt_rand(100000, 999999);
				
				$data1 = array('password'=>md5($rnd));
				$data = array('password'=>$rnd);
				if($this->api_m->insert($data1,'master',$id)){
					$this->api_m->send_email($email, $pwd = $rnd);
					$this->api_validate($error=FALSE,$status='success',$statuscode=102,$this->msg('pwd_changed'));	
				}else{
					$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('error_update'));
				}
			
			}else {
				
			$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('invalid_access'));	
			}// admin login end code
		}else{
		$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('invalid_type'));
		}
		
	}
}
	$this->output($data);
	}
}