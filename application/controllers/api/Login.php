<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller{

	protected $status = 'failed';
	protected $statuscode = '101';
	protected $error = TRUE;
	protected $message = 'Invalid Message';
	
	
    public function __construct() {
    	parent::__construct();
	    $this->load->model('Api_model','api_m');
	    $this->load->library('CommonExp', '', 'common');
	    //$this->load->library('CommonExp', '', 'common');
	}   
	protected function output($AP = array()){
		$opt = array(
			'error'=>$this->error,
			'status'=>$this->status,
			'statuscode'=>$this->statuscode,
			'message'=>$this->message
		);
		if(count($AP)){
		 	$opt = $opt+$AP;
		}
		echo json_encode($opt);
	}
		
	protected function api_validate($error,$status,$statuscode,$message){
		$this->error = $error;	
		$this->status = $status;	
		$this->statuscode = $statuscode;	
		$this->message = $message;		
	}
		
	protected function msg($key){
		return $this->common->exceptions()[$key];
	}
		
	public function index(){
		$data = array();
		$postdata =(array) json_decode(file_get_contents('php://input'));

	if(!empty($postdata )) {	
		if(!isset($postdata['email']) || $postdata['email'] == ''){	
			$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('email_required'));
		
		}elseif(!isset($postdata['password']) || $postdata['password']==''){		
			$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('password_required'));

		}else{		
			$LoginArr = array(
				'email' =>trim($postdata['email']),
				'password' =>md5(trim($postdata['password']))
			);
			$logindata = $this->api_m->secure_login($LoginArr,$table='master');
			if(!empty($logindata)){
				if($logindata->status =='Active'){
					$data['data'] =(array) $logindata;
					$this->api_validate($error=FALSE,$status='success',$statuscode=102,'Login success');	
				}else{
					$this->api_validate($error=true,$status='success',$statuscode=103,$this->msg('not_verified'));	
				}
			}else {
			$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('invalid_login'));	
			}
		}
	}
	$this->output($data);
	}
	public function register(){
		$postdata =(array) json_decode(file_get_contents('php://input'));
		if(!empty($postdata )) {
		if(!isset($postdata['name']) || $postdata['name']==''){
			$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'Name is required');
		}elseif(!isset($postdata['email']) || $postdata['email']==''){
			$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'Email is required');
		}elseif(!isset($postdata['phone']) || $postdata['phone']==''){
			$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'Phone is required');
		}elseif(!isset($postdata['address']) || $postdata['address']==''){
			$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'address is required');
		}elseif(!isset($postdata['password']) || $postdata['password']==''){
			$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'password is required');
		}else{
		
		$this->load->helper('common');
		if(!valid_email($postdata['email'])){
			$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'Email not valid');	
		}elseif($this->api_m->check_duplicacy('master',$column="email",$postdata['email'])){
			$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'Email already exits');	
		}else{
			$InsertArr = [];
			$InsertArr['onadd'] = date('Y-m-d H:i:s');
			$InsertArr['name'] = $postdata['name'];
			$InsertArr['email'] = $postdata['email'];
			$InsertArr['phone'] = $postdata['phone'];
			$InsertArr['address'] = $postdata['address'];
			$ins_id = $this->api_m->insert($InsertArr,$table='master',$id=Null);
					$s = referal_generation($ins_id);
					$this->api_m->insert(['referral_id'=>$s],'master',$ins_id);
			if($ins_id){
				$this->api_validate($error=FALSE,$status='success',$errorcode=102,'Inserted successfully');	
			}
		}
		}
		}
		$this->output();
	}
}