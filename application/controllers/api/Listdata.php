<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Listdata extends CI_Controller{

	protected $status = 'failed';
	protected $statuscode = '101';
	protected $error = TRUE;
	protected $message = 'Invalid Message';
	
	
    public function __construct() {
    parent::__construct();
    $this->load->model('Api_model','api_m');
    $this->load->library('CommonExp', '', 'common');
	$this->load->library('email');
	}   
	protected function output($AP = array()){
	$opt = array(
	'error'=>$this->error,
	'status'=>$this->status,
	'statuscode'=>$this->statuscode,
	'message'=>$this->message
	);
	if(count($AP)){
	 $opt = $opt+$AP;
	}
	echo json_encode($opt);
	}
	
	protected function api_validate($error,$status,$statuscode,$message){
	$this->error = $error;	
	$this->status = $status;	
	$this->statuscode = $statuscode;	
	$this->message = $message;		
	}
	
	protected function msg($key){
		return $this->common->exceptions()[$key];
	}
	
	public function index(){
		
	$data = array();
	$postdata =(array) json_decode(file_get_contents('php://input'));
	
	if(!empty($postdata )) {
			
			if(isset($postdata['type']) && $postdata['type']==''){
			
			$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('type'));

			}else{
			$type = $postdata['type'];
				if($type =='STUDENT'){
					$data = array('listdata'=>$this->api_m->get('student'));
					$this->api_validate($error=FALSE,$status='success',$statuscode=102,$this->msg('list_data'));	
			
				}else if($type=='FACULTY'){
					$data = array('listdata'=>$this->api_m->get('faculty'));
					$this->api_validate($error=FALSE,$status='success',$statuscode=102,$this->msg('list_data'));
					
				}else{
				$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('invalid_type'));
				}
				
			}
		}
	$this->output($data);
	}
	public function filter(){
		
	$data = array();
	$postdata =(array) json_decode(file_get_contents('php://input'));
	
	if(!empty($postdata )) {
			
			if(isset($postdata['type']) && $postdata['type']==''){
				$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('type'));
			}elseif(isset($postdata['field']) && $postdata['field']==''){
				$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('field_name'));
			}elseif(isset($postdata['value']) && $postdata['value']==''){
				$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('field_value'));
			}else{
				$type = $postdata['type'];
				$field = $postdata['field'];
				$value = $postdata['value'];
				if($type =='STUDENT'){
					if($this->api_m->db_fields_check('student',$field)){
					$data = array('listdata'=>$this->api_m->get_where('student',array($field => $value)));
					$this->api_validate($error=FALSE,$status='success',$statuscode=102,$this->msg('list_data'));
					}else{
						$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('field_name_v'));
					}
			
				}elseif($type=='FACULTY'){
					if($this->api_m->db_fields_check('student',$field)){
					$data = array('listdata'=>$this->api_m->get_where('faculty',array($field => $value)));
					$this->api_validate($error=FALSE,$status='success',$statuscode=102,$this->msg('list_data'));
					}else{
						$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('field_name_v'));
					}
					
				}else{
				$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('invalid_type'));
				}
				
			}
		}
	$this->output($data);
	}
}