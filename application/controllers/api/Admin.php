<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Controller{

	protected $status = 'failed';
	protected $errorcode = '101';
	protected $error = TRUE;
	protected $message = 'Invalid Message';
	
	
    public function __construct() {
    parent::__construct();
    $this->load->model('Api_model','api_m');
    $this->load->library('CommonExp', '', 'common');
	}   
	protected function output($AP = array()){
	$opt = array(
	'error'=>$this->error,
	'status'=>$this->status,
	'errorcode'=>$this->errorcode,
	'message'=>$this->message
	);
	if(count($AP)){
	 $opt = $opt+$AP;
	}
	echo json_encode($opt);
	}
	
	protected function api_validate($error,$status,$errorcode,$message){
	$this->error = $error;	
	$this->status = $status;	
	$this->errorcode = $errorcode;	
	$this->message = $message;		
	}
	
	protected function msg($key){
		return $this->common->exceptions()[$key];
	}
	
	public function login()
	{	 
	$data = array();
	$postdata =(array) json_decode(file_get_contents('php://input')); 
	if(!empty($postdata )) {
	if(isset($postdata['username']) && $postdata['username']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'Username Required');		
	}else if(isset($postdata['password']) && $postdata['password']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'Password Required');		
	}else{	
	 $adminlogin = $this->api_m->login(trim($postdata['username']),trim($postdata['password']));	 
	if(count($adminlogin)){	
	$data['logindata'] = $adminlogin;		
	$this->api_validate($error=FALSE,$status='success',$errorcode=102,'Login Success ');			
	}else{	
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,'Login Failed');			
	}	
	}
	} 
	$this->output($data);	
	}
}