<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Uploadimage extends CI_Controller{

	protected $status = 'failed';
	protected $statuscode = '101';
	protected $error = TRUE;
	protected $message = 'Invalid Message';
	
	
    public function __construct() {
    parent::__construct();
    $this->load->model('Api_model','api_m');
    $this->load->library('CommonExp', '', 'common');
	$this->load->library('email');
	}   
	protected function output($AP = array()){
	$opt = array(
	'error'=>$this->error,
	'status'=>$this->status,
	'statuscode'=>$this->statuscode,
	'message'=>$this->message
	);
	if(count($AP)){
	 $opt = $opt+$AP;
	}
	echo json_encode($opt);
	}
	
	protected function api_validate($error,$status,$statuscode,$message){
	$this->error = $error;	
	$this->status = $status;	
	$this->statuscode = $statuscode;	
	$this->message = $message;		
	}
	
	protected function msg($key){
		return $this->common->exceptions()[$key];
	}
	
	public function index(){
		
	$data = array();
	$postdata =(array) json_decode(file_get_contents('php://input'));
	
	if(!empty($postdata )) {
	
	if(isset($postdata['image']) && $postdata['image']==''){
	
	$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('email_required'));
	
	}else if(isset($postdata['usertype']) && $postdata['usertype']==''){
	
	$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('usertype_required'));

	}else{
		
	$image =trim($postdata['image']);
	$type = trim($postdata['usertype']);
	
		if($type =='STUDENT'){
		
			//$postdata =(array) json_decode(file_get_contents('php://input'));
			$data1 = $image;
			$data1 = base64_decode($data1); // base64 decoded image data
			$source_img = imagecreatefromstring($data1);
			$rotated_img = imagerotate($source_img, 0, 0); // rotate with angle 90 here
			$file = 'assets/images/'. uniqid() . '.png';
			$imageSave = imagejpeg($rotated_img, $file, 100);
			imagedestroy($source_img);
			//echo "success";
				
			$this->api_validate($error=FALSE,$status='Success',$statuscode=102,$this->msg('success_upload'));	
		
		}else if($type=='FACULTY'){
				// faculty login start code
			
		}else if($type=='ADMIN'){
				// admin login start code
				
		}else{
		$this->api_validate($error=TRUE,$status='failed',$statuscode=101,$this->msg('invalid_type'));
		}
		
	}
}
	$this->output($data);
	}
}