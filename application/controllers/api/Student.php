<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Student extends CI_Controller{

	protected $status = 'failed';
	protected $errorcode = '101';
	protected $error = TRUE;
	protected $message = 'Invalid Message';
	
	
    public function __construct() {
    parent::__construct();
    $this->load->model('Api_model','api_m');
    $this->load->library('CommonExp', '', 'common');
    $this->load->library('email');
	}   
	protected function output($AP = array()){
	$opt = array(
	'error'=>$this->error,
	'status'=>$this->status,
	'errorcode'=>$this->errorcode,
	'message'=>$this->message
	);
	if(count($AP)){
	 $opt = $opt+$AP;
	}
	echo json_encode($opt);
	}
	
	protected function api_validate($error,$status,$errorcode,$message){
	$this->error = $error;	
	$this->status = $status;	
	$this->errorcode = $errorcode;	
	$this->message = $message;		
	}
	
	protected function msg($key){
		return $this->common->exceptions()[$key];
	}
	
	public function add_student(){
	$postdata =(array) json_decode(file_get_contents('php://input'));
	if(!empty($postdata )) {
	if(isset($postdata['username']) && $postdata['username']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('empty_student_name'));
	}else if(isset($postdata['father_name']) && $postdata['father_name']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['mother_name']) && $postdata['mother_name']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['email']) && $postdata['email']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('empty_student_email'));
	}else if(isset($postdata['password']) && $postdata['password']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['class']) && $postdata['class']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['local_gurdian']) && $postdata['local_gurdian']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['gender']) && $postdata['gender']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['phone']) && $postdata['phone']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('empty_student_phone'));
	}else if(isset($postdata['dob']) && $postdata['dob']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['address']) && $postdata['address']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['city']) && $postdata['city']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['pincode']) && $postdata['pincode']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['college']) && $postdata['college']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['courses']) && $postdata['courses']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['total_amount']) && $postdata['total_amount']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else if(isset($postdata['addmisstion_date']) && $postdata['addmisstion_date']==''){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('please_fill_field'));
	}else {
		//profile pic insert code
		$file_pic = '';		
		if(isset($postdata['profile_pic']) && $postdata['profile_pic'] !=''){
			//echo "hello";
			$data1 = $postdata['profile_pic'];
			$data1 = base64_decode($data1); // base64 decoded image data
			$source_img = imagecreatefromstring($data1);
			$rotated_img = imagerotate($source_img, 0, 0); // rotate with angle 90 here
			$file_pic = 'assets/images/'. uniqid() . '.png';
			$imageSave = imagejpeg($rotated_img, $file_pic, 100);
			imagedestroy($source_img);
			$file_pic = base_url().$file_pic;
		}
		//profile pic insert code ends
		
	$InsertArr = $this->api_m->getapidata($postdata);
	$InsertArr['onadd'] = date('Y-m-d H:i:s');
	$InsertArr['profile_pic'] = $file_pic;
	if($this->api_m->check_duplicacy($table="student",$column="email",$InsertArr['email'])){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('email_exists'));	
	}else if($this->api_m->check_duplicacy($table="student",$column="phone",$InsertArr['phone'])){
	$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('phone_exists'));	
	}else{
	if($this->api_m->insert($InsertArr,$table='student',$id=Null)){
	$this->api_validate($error=FALSE,$status='success',$errorcode=102,$this->msg('add_student'));	
	}
	}
	}
	}
	$this->output();
	}
	
	public function update_student(){
		$data_ins = array();
		$postdata =(array) json_decode(file_get_contents('php://input'));
	if(!empty($postdata )) {
		if(isset($postdata['username']) && $postdata['username']!=''){
			$data_ins['username'] = $postdata['username'];
		}
		if(isset($postdata['father_name']) && $postdata['father_name']!=''){
			$data_ins['father_name'] = $postdata['father_name'];
		}
		if(isset($postdata['mother_name']) && $postdata['mother_name']!=''){
			$data_ins['mother_name'] = $postdata['mother_name'];
		}
		if(isset($postdata['email']) && $postdata['email']!=''){
			$data_ins['email'] = $postdata['email'];
			if($this->api_m->check_duplicacy($table="student",$column="email",$data_ins['email'])){
				$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('email_exists'));	
				$this->output();
				exit();
			}
		}
		if(isset($postdata['password']) && $postdata['password']!=''){
			$data_ins['password'] = $postdata['password'];
		}
		if(isset($postdata['class']) && $postdata['class']!=''){
			$data_ins['class'] = $postdata['class'];
		}
		if(isset($postdata['local_gurdian']) && $postdata['local_gurdian']!=''){
			$data_ins['local_gurdian'] = $postdata['local_gurdian'];
		}
		if(isset($postdata['gender']) && $postdata['gender']!=''){
			$data_ins['gender'] = $postdata['gender'];
		}
		if(isset($postdata['phone']) && $postdata['phone']!=''){
			$data_ins['phone'] = $postdata['phone'];
		}
		if(isset($postdata['dob']) && $postdata['dob']!=''){
			$data_ins['dob'] = $postdata['dob'];
		}
		if(isset($postdata['address']) && $postdata['address']!=''){
			$data_ins['address'] = $postdata['address'];
		}
		if(isset($postdata['city']) && $postdata['city']!=''){
			$data_ins['city'] = $postdata['city'];
		}
		if(isset($postdata['pincode']) && $postdata['pincode']!=''){
			$data_ins['pincode'] = $postdata['pincode'];
		}
		if(isset($postdata['college']) && $postdata['college']!=''){
			$data_ins['college'] = $postdata['college'];
		}
		if(isset($postdata['courses']) && $postdata['courses']!=''){
			$data_ins['courses'] = $postdata['courses'];
		}
		if(isset($postdata['total_amount']) && $postdata['total_amount']!=''){
			$data_ins['total_amount'] = $postdata['total_amount'];
		}
		if(isset($postdata['addmisstion_date']) && $postdata['addmisstion_date']!=''){
			$data_ins['addmisstion_date'] = $postdata['addmisstion_date'];
		}
		//profile pic insert code		
		if(isset($postdata['profile_pic']) && $postdata['profile_pic'] !=''){
			//echo "hello";
			$data1 = $postdata['profile_pic'];
			$data1 = base64_decode($data1); // base64 decoded image data
			$source_img = imagecreatefromstring($data1);
			$rotated_img = imagerotate($source_img, 0, 0); // rotate with angle 90 here
			$file_pic = 'assets/images/'. uniqid() . '.png';
			$imageSave = imagejpeg($rotated_img, $file_pic, 100);
			imagedestroy($source_img);
			$file_pic = base_url().$file_pic;
			$data_ins['profile_pic'] = $file_pic;
		}
		//profile pic insert code ends
		//print_r($data_ins);
		if(isset($postdata['id']) && $postdata['id'] !=''){
		$data_ins['onupdate'] = date('Y-m-d H:i:s');
			if(isset($postdata['status']) && $postdata['status'] =='Active'){
				$res = $this->api_m->get_where('student',array('id'=>$postdata['id']));
				$email = isset($postdata['email']) ? $postdata['email'] : $res[0]->email;
				$rnd =  mt_rand(100000, 999999);
				$data_ins['password'] = $rnd;
				$data_ins['status'] = $postdata['status'];
				$this->api_m->send_email_user($email, 'Your account activated now please login with \n Email : '.$email.' Password : '.$rnd);
			}
			//print_r($data_ins);
		if($this->api_m->insert($data_ins,$table='student',$id=$postdata['id'])){
			$this->api_validate($error=FALSE,$status='success',$errorcode=102,$this->msg('update_student'));	
		}else{
			$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('error_s'));	
		}
		
	}else{
		$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('id_required'));			
	}
		}
	
	$this->output();
	}
	public function delete_student(){
		$data_ins = array();
		$postdata =(array) json_decode(file_get_contents('php://input'));
	if(!empty($postdata )) {
		if(isset($postdata['id']) && $postdata['id'] ==''){
			$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('id_required'));	
		}else{
				if($this->api_m->delete($table ='student',$postdata['id'])){
					$this->api_validate($error=FALSE,$status='Success',$errorcode=102,$this->msg('del_success'));	
				}else{
					$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('error_s'));	
				}
		}
		
	}else{
		$this->api_validate($error=TRUE,$status='failed',$errorcode=101,$this->msg('error_s'));			
	}
		
		
	
	$this->output();
	}
	function saveImage($data, $filename) {

    $base64_img_array = explode(':', $data);

    $img_info = explode(',', end($base64_img_array));
    $img_file_extension = '';
    if (!empty($img_info)) {
        switch ($img_info[0]) {
            case 'image/jpeg;base64':
                $img_file_extension = 'jpeg';
                break;
            case 'image/jpg;base64':
                $img_file_extension = 'jpg';
                break;
            case 'image/gif;base64':
                $img_file_extension = 'gif';
                break;
            case 'image/png;base64':
                $img_file_extension = 'png';
                break;
        }
    }
    $img_file_name = 'assets/images/' . $filename . '.' . $img_file_extension;    
    $img_file = file_put_contents($img_file_name, base64_decode($img_info[1]));

    if ($img_file) {
        return $img_file_name;
    } else {
        return false;
    }
}

}