<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	 public function __construct(){
		parent::__construct();
		$data = array();
		$this->load->model('master_model','master_m');
		$this->load->library('form_validation');
        if($this->master_m->isloggedIn() == FALSE ){ 
         	redirect('login/index');
        }
  	
	}
	public function user_profile(){
		$user_id = $this->master_m->get_current_user_id();
		$data['title']='Profile';
		$data['details'] = $this->master_m->get('master',$user_id);
		
		$this->load->view('elements/header',$data);
		$this->load->view('users/user_profile',$data);
		$this->load->view('users/modal',$data);
		$this->load->view('elements/footer');
	}
	public function update_user_profile(){
		if ($this->input->is_ajax_request()) {
			$user_id = $this->master_m->get_current_user_id();
			$InsertArr = $this->master_m->getposteddata(array('name','dob','gender','address','phone'));		
			$ms_check = $this->master_m->insert($InsertArr,'master',$user_id);
			if($ms_check){
				echo json_encode(array('error'=>false,'msg'=>'success','url'=>base_url().'index.php/user/user_profile'));
			}
			exit();
		}
	}
	public function update_password(){

		if ($this->input->is_ajax_request()) {			
			$user_id = $this->master_m->get_current_user_id();
			$old_password = $_POST['oldpassword'];
			$new_password = $_POST['newpassword'];
			$confirm_password = $_POST['confirmpassword'];
			if($new_password != $confirm_password){
				echo json_encode(array('error'=>true,'msg'=>'Password & Confirm Password not Matched'));
			}else{
				$check = $this->master_m->get_where('master',['id'=>$user_id,'password'=>md5($old_password)]);
				if(!empty($check)){
					$ms_check = $this->master_m->insert(['password'=>md5($new_password)],'master',$user_id);
					echo json_encode(array('error'=>false,'msg'=>'Successfully Updated','url'=>base_url().'index.php/user/user_profile'));
				}else{
					echo json_encode(array('error'=>true,'msg'=>'Old Password Not Matched'));
				}
			}
			exit();
		}
	}
	public function manage_User()
		{
		  	$data['title']='Manage User';
		 	$data['User'] = $this->master_m->get_where($table ='master',['status !='=>'Delete']) ;
		    $this->load->view('elements/header',$data);
			$this->load->view('users/manage_user',$data);
			$this->load->view('elements/footer');
		}
	//User starts
	public function add_User($id=null)
    {
    	$data['title']='New User';
		if($id){
			$data['title']='Update User';
			$data['User'] = $this->master_m->get($table ='master',$id) ;
		}else{
			$data['User'] = $this->master_m->get_new(array('id','name','email','phone','address','dob','gender','status')) ;
		}
		
		
		if($this->input->server('REQUEST_METHOD')=='POST'){
			
			$InsertArr = $this->master_m->getposteddata(array('name','email','phone','address','dob','gender','status'));		
			$InsertArr['onadd']= $id ? $data['User']->onadd : date('Y-m-d H:i:s');
			
			if($this->input->post('hemail') != $this->input->post('email')){
				$check =  $this->master_m->get_where('master',array('email'=>$this->input->post('email')));
				if(empty($check)){
					$ins_id = $this->master_m->insert($InsertArr, $table='master', $id);
					if ($id == '') {
						$s = referal_generation($ins_id);
						$this->master_m->insert(['referral_id'=>$s], $table='master',$ins_id);
						//exit;
					}
					redirect('user/manage_User');
				}else{
					 $this->session->set_flashdata('error', array('msg'=>'This email already exists','type'=>'danger')); 
					redirect('user/add_User/'.$id);
				}
				
			}else{
				$ins_id = $this->master_m->insert($InsertArr, $table='master', $id);
				if ($id == '') {
					$s = referal_generation($ins_id);
					$this->master_m->insert(['referral_id'=>$s], $table='master',$ins_id);
					//exit;
				}
				if($ins_id){
					redirect('user/manage_user');
				}
			}
		} 
		$this->load->view('elements/header',$data);
		$this->load->view('users/add_user',$data);
		$this->load->view('elements/footer');
    }
	
	
	public function delete_User($id)
    {
		if($id){
			if($this->master_m->insert(['status'=>'Delete'],'master',$id)){
				redirect('user/manage_user');		
			}	 
		}
    }
	
}