<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller {
	 public function __construct(){
		parent::__construct();
		$data = array();
		$this->load->model('master_model','master_m');
		$this->load->library('form_validation');
        if($this->master_m->isloggedIn() == FALSE ){ 
         	redirect('login/index');
        }
  	
	}
	public function index()
		{
		  $data['title']='Manage Category';
	 	  $data['plans'] = $this->master_m->get_where($table ='category',['status !='=>'Delete']) ;
		 // $data['pathology'] = $this->master_m->get_pathology_report() ;
		    $this->load->view('elements/header',$data);
			$this->load->view('category/manage_category',$data);
			$this->load->view('elements/footer');
		}
	
	// Plans Category starts
	public function add_category($id=null)
    {
	$data['title']='Add New Category';
		
		if($id){
			$data['plans'] = $this->master_m->get($table ='category',$id) ;
			$data['title']='Edit Category';
		}else{
			$data['plans'] = $this->master_m->get_new(array('id','name','status','description')) ;	
		}
		
		if($this->input->server('REQUEST_METHOD')=='POST'){			
			$InsertArr = $this->master_m->getposteddata(array('name','status','description'));		
			$InsertArr['onadd']= $id ? $data['plans']->onadd : date('Y-m-d H:i:s');
			if($this->master_m->insert($InsertArr, $table='category', $id)){
				redirect('category/index');
			}
		} 
		
		$this->load->view('elements/header',$data);
		$this->load->view('category/add_category',$data);
		$this->load->view('elements/footer');
    }
		public function delete_category($id)
	    {

			if($id){
			if($this->master_m->insert(['status'=>'Delete'],'category',$id)){
			redirect('category/index');		
			}	 
			}
	    }

    public function manage_subcategory()
		{
		  $data['title']='Manage sub Category';
	 	  $data['plans'] = $this->master_m->get_subcategory() ;
		 // $data['pathology'] = $this->master_m->get_pathology_report() ;
		    $this->load->view('elements/header',$data);
			$this->load->view('category/manage_subcategory',$data);
			$this->load->view('elements/footer');
		}
	
	// Plans Category starts
	public function add_subcategory($id=null)
    {
		$data['title']='Add New Sub Category';
		$data['category']=$this->master_m->get('category');
		
		if($id){
			$data['plans'] = $this->master_m->get_subcategory($id) ;
			$data['title']='Edit Category';
		}else{
			$data['plans'] = $this->master_m->get_new(array('id','category_id','name','status','description')) ;	
		}
		
		if($this->input->server('REQUEST_METHOD')=='POST'){			
			$InsertArr = $this->master_m->getposteddata(array('name','category_id','status','description'));		
			$InsertArr['onadd']= $id ? $data['plans']->onadd : date('Y-m-d H:i:s');
			if($this->master_m->insert($InsertArr, $table='subcategory', $id)){
				redirect('category/manage_subcategory');
			}
		} 
		
		$this->load->view('elements/header',$data);
		$this->load->view('category/add_subcategory',$data);
		$this->load->view('elements/footer');
    }

	


    public function delete_subcategory($id)
    {
		if($id){
			if($this->master_m->insert(['status'=>'Delete'],'subcategory',$id)){
				redirect('category/manage_subcategory');		
			}	 
		}
    }
	
	
	
	
	
	
}