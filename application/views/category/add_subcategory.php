<section class="content">
        <div class="row">
            <!-- Form controls -->
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="btn-group"> 
                            <a class="btn btn-primary btn-list" href="<?php echo base_url().'index.php/category/index/'?>"> <i class="fa fa-list"></i> Sub Category List</a>                       
                        </div>
                  </div>
                  <div class="panel-body">
                      <form action="<?php echo base_url().'index.php/category/add_subcategory/'.$plans->id; ?>" method="post">
					   <?php if($this->session->flashdata('error')){ ?>
								<div class="alert alert-<?php echo $this->session->flashdata('error')['type']; ?>">
									<button class="close" data-close="alert"></button>
									<span><?php echo $this->session->flashdata('error')['msg']; ?></span>
								</div>
					<?php } ?>
                        <div class="form-group">
                          <label class="form-control-label">Category Name</label>
                          <select name="category_id" class="form-control" required>
                              <?php if (!empty($category)) { 
                                    foreach ($category as $value) {
                                      $sele = $value->id == $plans->category_id ?'selected':'';
                               ?>
                                <option value="<?php echo $value->id;?>" <?php echo $sele;?>>
                                  <?php echo $value->name;?>
                                    
                                  </option>
                                <?php } } ?>
                                
                          </select>
                        </div>
                        <div class="form-group">
                          <label class="form-control-label">Sub Category Name</label>
                          <input type="text" name="name" value="<?php echo $plans->name; ?>" placeholder="Enter Category Name" class="form-control" required>
                        </div>
						<div class="form-group">       
                          <label class="form-control-label">Sub Category Description</label>
                          <textarea  placeholder="Enter Category Description" name="description"  class="form-control"><?php echo $plans->description; ?></textarea>
                        </div>					
						
						 <div class="form-group">       
                          <label class="form-control-label">Status</label>
                          <select name="status" class="form-control" required>
                              <option value="">Select</option>
                              <option  <?php if($plans->status == "Active") echo "selected='selected'"; ?>>Active</option>
                              <option <?php if($plans->status == "Inactive") echo "selected='selected'"; ?>>Inactive</option>
                              
                            </select>
                        </div>
                        <div class="form-group">       
                          <input type="submit" value="<?php if($plans->id =='') echo 'Submit'; else echo 'Update'?>" class="btn btn-primary">
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
     </div>
 </section> <!-- /.content -->
