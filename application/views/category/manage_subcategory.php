<section class="content">
        <div class="row">
            <!-- Form controls -->
            <div class="col-sm-12">
                <div class="panel panel-bd lobidisable">
                    <div class="panel-heading">
                        <div class="btn-group"> 
                          <a class="btn btn-primary btn-list" href="<?php echo base_url().'index.php/category/add_subcategory/'?>"> 
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New Sub Category</a>
                          
                      </div>
                  </div>
                  <div class="panel-body">
                      <table class="table table-striped responsive table-sm data-table">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Category Name</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
						<?php $i =1;
						if(!empty($plans)){
							foreach($plans as $dd){?>
                          <tr>
                            <th scope="row"><?php echo $i; ?></th>
                            <td><?php echo $dd->category_name; ?></td>
                            <td><?php echo $dd->name; ?></td>
                            <td><?php echo $dd->description; ?></td>
                            <td>
							<span class="label <?php if($dd->status == 'Active') echo 'label-success label-default'; else echo 'label-default label-danger';?>"><?php echo $dd->status; ?></span>
							</td>
                            <td>
											
								<a data-toggle="tooltip" data-placement="left" title="" data-original-title="Update" href="<?php echo base_url().'index.php/category/add_subcategory/'.$dd->id; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a data-toggle="tooltip" data-placement="left" title="" data-original-title="Delete" href="<?php echo base_url().'index.php/category/delete_subcategory/'.$dd->id; ?>" class="btn btn-danger btn-xs Confirm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								
							</td>
                          </tr>
						<?php $i++;
						}} ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
		
		 </div>
     </div>
 </section> <!-- /.content -->
         