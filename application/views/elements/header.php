<!DOCTYPE html>
<html lang="en">
<?php $user_session = $this->session->userdata('admin'); 
      $user_details = get_user_data_by_id($user_session['userid']);
     $uri_segment1 = $this->uri->segment(1); 
     $uri_segment2 = $this->uri->segment(2); 
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/ico/favicon.ico" type="image/x-icon">
   <!-- Start Global Mandatory Style
   =====================================================================-->
   <!-- jquery-ui css -->
   <link href="<?php echo base_url(); ?>assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
   <!-- Bootstrap -->
   <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
   <!-- Bootstrap rtl -->
   <!--<link href="assets/bootstrap-rtl/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>-->
   <!-- Lobipanel css -->
   <link href="<?php echo base_url(); ?>assets/plugins/lobipanel/lobipanel.min.css" rel="stylesheet" type="text/css"/>
   <!-- Pace css -->
   <link href="<?php echo base_url(); ?>assets/plugins/pace/flash.css" rel="stylesheet" type="text/css"/>
   <!-- Font Awesome -->
   <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
   <!-- Pe-icon -->
   <link href="<?php echo base_url(); ?>assets/pe-icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet" type="text/css"/>
   <!-- Themify icons -->
   <link href="<?php echo base_url(); ?>assets/themify-icons/themify-icons.css" rel="stylesheet" type="text/css"/>
        <!-- End Global Mandatory Style
        =====================================================================-->
        <!-- Start page Label Plugins 
        =====================================================================-->
        <!-- Toastr css -->
        <link href="<?php echo base_url(); ?>assets/plugins/toastr/toastr.css" rel="stylesheet" type="text/css"/>
        <!-- Emojionearea -->
        <link href="<?php echo base_url(); ?>assets/plugins/emojionearea/emojionearea.min.css" rel="stylesheet" type="text/css"/>
        <!-- Monthly css -->
        <link href="<?php echo base_url(); ?>assets/plugins/monthly/monthly.css" rel="stylesheet" type="text/css"/>
        <!-- End page Label Plugins 
        =====================================================================-->
        <!-- Start Theme Layout Style
        =====================================================================-->
        <!-- Theme style -->
        <link href="<?php echo base_url(); ?>assets/dist/css/stylehealth.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/dist/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/dist/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css"/>
        <!--<link href="assets/dist/css/stylehealth-rtl.css" rel="stylesheet" type="text/css"/>-->
        <!-- End Theme Layout Style
        =====================================================================-->
           <!-- jQuery -->
        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-1.12.4.min.js" type="text/javascript"></script>
    </head>
    <body class="hold-transition sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
                <a href="<?php echo base_url(); ?>index.php/master/index" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <!--<b>A</b>H-admin-->
                        <img src="<?php echo base_url(); ?>assets/dist/img/icons8-dumbbell-64.png" alt="">
                    </span>
                    <span class="logo-lg">
                        <!--<b>Admin</b>H-admin-->
                        <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="">
                    </span>
                </a>
                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top ">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <!-- Sidebar toggle button-->
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-tasks"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="pe-7s-mail"></i>
                                    <span class="label label-success">2</span>
                                </a>
                                
                                <ul class="dropdown-menu">
                                    <li class="header"><i class="fa fa-envelope-o"></i>
                                    2 Messages</li>
                                    <li>
                                        <ul class="menu">
                                            <li><!-- start message -->
                                               <a href="#" class="border-gray">
                                                    <div class="pull-left">
                                                    <img src="<?php echo base_url(); ?>assets/dist/img/avatar2.png" class="img-thumbnail" alt="User Image"></div>
                                                    <h4>Alrazy</h4>
                                                    <p>Lorem Ipsum is simply dummy text of...
                                                    </p>
                                                    <span class="label label-success pull-right">11.00am</span>
                                                </a>       

                                            </li>
                                            <li>
                                                <a href="#" class="border-gray">
                                                    <div class="pull-left">
                                                    <img src="<?php echo base_url(); ?>assets/dist/img/avatar4.png" class="img-thumbnail" alt="User Image"></div>
                                                    <h4>Tanjil</h4>
                                                    <p>Lorem Ipsum is simply dummy text of...
                                                    </p>
                                                    <span class="label label-success pull-right"> 12.00am</span>
                                                </a>       

                                            </li>
                                            
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">See all messages <i class=" fa fa-arrow-right"></i></a>
                                    </li>
                                </ul>
                            </li>
                            
                            <!-- Tasks -->
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="pe-7s-file"></i>
                                    <span class="label label-danger">2</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><i class="fa fa-file"></i> 2 tasks</li>
                                    <li>
                                        <ul class="menu">
                                            <li> <!-- Task item -->
                                                <a href="#">
                                                    <h3>
                                                        <i class="fa fa-check-circle"></i> Data table error
                                                        <span class="label-primary label label-default pull-right">35%</span>
                                                    </h3>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" data-original-title="35%" style="width: 35%">
                                                            <span class="sr-only">35% Complete (primary)</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li> <!-- end task item -->
                                            <li> <!-- Task item -->
                                                <a href="#">
                                                    <h3>
                                                      <i class="fa fa-check-circle"></i>  Change theme color
                                                       <span class="label-success label label-default pull-right">55%</span>
                                                    </h3>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" data-original-title="55%" style="width: 55%">
                                                            <span class="sr-only">55% Complete (primary)</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li> <!-- end task item -->
                                            
                                            <!-- end task item -->
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">See all tasks <i class=" fa fa-arrow-right"></i></a></li>
                                </ul>

                            </li>
                            <!-- user -->
                            <li class="dropdown dropdown-user admin-user">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                                <div class="user-image">
                                <img src="<?php echo base_url(); ?>assets/dist/img/avatar4.png" class="img-circle" height="40" width="40" alt="User Image">
                                </div>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo base_url().'index.php/user/user_profile';?>"><i class="fa fa-users"></i> User Profile</a></li>
                                    
                                    <li><a href="<?php echo base_url().'index.php/login/logout'; ?>"><i class="fa fa-sign-out"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- =============================================== -->
            <!-- Left side column. contains the sidebar -->
          <aside class="main-sidebar">
                <!-- sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="image pull-left">
                            <img src="<?php echo base_url(); ?>assets/dist/img/avatar5.png" class="img-circle" alt="User Image">
                        </div>
                        <div class="info">
                            <?php ;
                           // pr($user_details); ?>
                            <h4>Welcome</h4>
                            <p><?php echo @$user_details->name;?></p>
                        </div>
                    </div>
                   
                    <!-- sidebar menu -->
                    <ul class="sidebar-menu">
                        <li class="">
                            <a href="<?php echo base_url().'index.php/master/index'; ?>"><i class="fa fa-hospital-o"></i><span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview <?php echo $uri_segment1=='user'?'active':'';?>">
                            <a href="#">
                                <i class="fa fa-user-md"></i><span>User</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo $uri_segment2=='add_user'?'active':'';?>">
                                    <a href="<?php echo base_url().'index.php/user/add_user'; ?>">
                                    Add User
                                    </a>
                                </li>
                                <li class="<?php echo $uri_segment2=='manage_user'?'active':'';?>">
                                    <a href="<?php echo base_url().'index.php/user/manage_user';?>">User list
                                    </a>
                                </li>
                                
                            </ul>
                        </li>
						
						<li class="treeview <?php echo $uri_segment1=='category' && ($uri_segment2=='add_category'|| $uri_segment2=='index')?'active':'';?>">
                            <a href="#">
                                <i class="fa fa-user-md"></i><span>Category</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo $uri_segment2=='add_category'?'active':'';?>">
                                    <a href="<?php echo base_url().'index.php/category/add_category'; ?>">Add Category</a>
                                </li>
                                <li class="<?php echo $uri_segment1.$uri_segment2=='categoryindex'?'active':'';?>">
                                    <a href="<?php echo base_url().'index.php/category/index'; ?>">Manage Category</a>
                                </li>
                                
                            </ul>
                        </li>

                        <li class="treeview <?php echo $uri_segment1=='category' && ($uri_segment2=='add_subcategory'|| $uri_segment2=='manage_subcategory')?'active':'';?>">
                            <a href="#">
                                <i class="fa fa-user-md"></i><span>Sub Category</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo $uri_segment2=='add_subcategory'?'active':'';?>">
                                    <a href="<?php echo base_url().'index.php/category/add_subcategory'; ?>">Add Sub Category</a>
                                </li>
                                <li class="<?php echo $uri_segment2=='manage_subcategory'?'active':'';?>">
                                    <a href="<?php echo base_url().'index.php/category/manage_subcategory'; ?>">Manage Sub Category</a>
                                </li>
                                
                            </ul>
                        </li>
                        <li class="treeview <?php echo $uri_segment1=='workouts'?'active':'';?>">
                            <a href="#">
                                <i class="fa fa-life-ring"></i><span>Work Outs</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo $uri_segment2=='add_workouts'?'active':'';?>">
                                    <a href="<?php echo base_url().'index.php/workouts/add_workouts'; ?>">Add Work Outs</a>
                                </li>
                                <li class="<?php echo $uri_segment1.$uri_segment2=='workoutsindex'?'active':'';?>">
                                    <a href="<?php echo base_url().'index.php/workouts/index'; ?>">Manage Work Outs</a>
                                </li>
                                
                            </ul>
                        </li>
                        <li class="treeview <?php echo $uri_segment1=='master' && $uri_segment2=='feedback'?'active':'';?>">
                            <a href="#">
                                <i class="fa fa-comments-o"></i><span>Feedback</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                               
                                <li class="<?php echo $uri_segment2=='feedback'?'active':'';?>">
                                    <a href="<?php echo base_url().'index.php/master/feedback'; ?>">Manage Feedback</a>
                                </li>
                                
                            </ul>
                        </li>
                        <li class="treeview  <?php echo $uri_segment1=='offers'?'active':'';?>">
                            <a href="#">
                                <i class="fa fa-life-ring"></i><span>Offers & Discounts</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo $uri_segment2=='add_offers'?'active':'';?>">
                                    <a href="<?php echo base_url().'index.php/offers/add_offers'; ?>">Add Offers</a>
                                </li>
                                <li class="<?php echo $uri_segment1.$uri_segment2=='offersindex'?'active':'';?>"> 
                                    <a href="<?php echo base_url().'index.php/offers/index'; ?>">Manage Offers</a>
                                </li>
                                
                            </ul>
                        </li>
                      </ul>
        </div> <!-- /.sidebar -->
    </aside>
            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                     <form action="#" method="get" class="sidebar-form search-box pull-right hidden-md hidden-lg hidden-sm">
                            <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>   
                    <div class="header-icon">
					<?php if($title =='Dashboard'){?>
                        <i class="fa fa-tachometer"></i>
					<?php }else { ?>
					<i class="fa fa-bandcamp" aria-hidden="true"></i>
					<?php }?>
                    </div>
                    <div class="header-title">
                        <h1> <?php  echo $title; ?></h1>
                        <small> admin features</small>
                        <ol class="breadcrumb hidden-xs">
                            <li><a href="<?php echo base_url().'index.php/master/index'; ?>"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active"><?php  echo $title; ?></li>
                        </ol>
                    </div>
                </section>
          