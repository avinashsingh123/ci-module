<section class="content">
        <div class="row">
            <!-- Form controls -->
            <?php  // pr($workouts);;?>
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="btn-group"> 
                          <a class="btn btn-primary btn-list" href="<?php echo base_url().'index.php/workouts/index/'?>"> <i class="fa fa-list"></i> Workouts List</a>
                          
                      </div>
                  </div>
                  <div class="panel-body">
                      <form action="<?php echo base_url().'index.php/workouts/add_workouts/'.$workouts->id; ?>" method="post">
					   <?php 

            
             if($this->session->flashdata('error')){ ?>
								<div class="alert alert-<?php echo $this->session->flashdata('error')['type']; ?>">
									<button class="close" data-close="alert"></button>
									<span><?php echo $this->session->flashdata('error')['msg']; ?></span>
								</div>
					<?php } ?>
                        <div class="form-group">
                          <label class="form-control-label">Category Name</label>
                          <select name="category_id" class="form-control" id="category_id" required>
                              <?php 
                               $sel_id = '';
                              if (!empty($category)) { 
                                $ct = 1;
                                    foreach ($category as $value) {
                                      $sele = $value->id == $workouts->category_id ?'selected':'';
                                      if (($value->id == $workouts->category_id) || ($ct == 1 && $workouts->category_id == '')) {
                                          $sel_id =  $value->id;
                                       } 
                               ?>
                                <option value="<?php echo $value->id;?>" <?php echo $sele;?>>
                                  <?php echo $value->name;?>
                                    
                                  </option>
                                <?php $ct++;} } ?>                                
                          </select>
                        </div>
                        <div class="form-group">
                          <label class="form-control-label">Sub Category Name</label>
                          <select name="subcategory_id" class="form-control" id="subcategory_id" required>
                              <?php 
                             
                              if (!empty($subcategory)) { 
                                    foreach ($subcategory as $value) {
                                      $sele = $value->id == $workouts->subcategory_id ?'selected':'';
                                      if ( $sel_id !=$value->category_id) {
                                       continue;
                                      }
                               ?>
                                <option value="<?php echo $value->id;?>" <?php echo $sele;?>>
                                  <?php echo $value->name;?>
                                    
                                  </option>
                                <?php } } ?>
                                
                          </select>
                        </div> 

                        <div class="form-group">
                          <label class="form-control-label">Workout Name</label>
                          <input type="text" name="name" value="<?php echo $workouts->name; ?>" placeholder="Enter Category Name" class="form-control">
                        </div>
						<div class="form-group">       
                          <label class="form-control-label">Category Description</label>
                          <textarea  placeholder="Enter Category Description" name="description"  class="form-control"><?php echo $workouts->description; ?></textarea>
                        </div>					
						
						 <div class="form-group">       
                          <label class="form-control-label">Status</label>
                          <select name="status" class="form-control">
                              <option value="">Select</option>
                              <option  <?php if($workouts->status == "Active") echo "selected='selected'"; ?>>Active</option>
                              <option <?php if($workouts->status == "Inactive") echo "selected='selected'"; ?>>Inactive</option>
                              
                            </select>
                        </div>
                        <div class="form-group">       
                          <input type="submit" value="<?php if($workouts->id =='') echo 'Submit'; else echo 'Update'?>" class="btn btn-primary">
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
     </div>
 </section> <!-- /.content -->
 <script type="text/javascript">
   $(document).ready(function(){
           
            $("#category_id").change(function(){
                code = $(this).val();
                $.ajax({
                    /* the route pointing to the post function */
                    url: '<?php echo base_url()."index.php/workouts/get_subcategory"; ?>',
                    type: 'POST',
                    data: {code:code},
                    dataType: 'JSON',
                    success: function (data) { 
                        var model = $('#subcategory_id');
                            model.empty();                                   
                            $.each(data, function(index, element) {
                                model.append("<option value='"+ index +"'>" + element + "</option>");
                            });
                          
                    }
                }); 
            }); 

          });
 </script>
