 <section class="content">
        <div class="row">
            <!-- Form controls -->
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="btn-group"> 
                          <a class="btn btn-primary btn-list" href="<?php echo base_url().'index.php/user/manage_user/'?>">
                           <i class="fa fa-list"></i> Users list</a>
                          
                      </div>
                  </div>
                  <div class="panel-body">
					<form action="<?php echo base_url().'index.php/user/add_user/'.$User->id; ?>" method="post">
					   <?php if($this->session->flashdata('error')){ ?>
								<div class="alert alert-<?php echo $this->session->flashdata('error')['type']; ?>">
									<button class="close" data-close="alert"></button>
									<span><?php echo $this->session->flashdata('error')['msg']; ?></span>
								</div>
					<?php } ?>
                        <div class="form-group">
                          <label class="form-control-label">Name</label>
                          <input type="text" name="name" value="<?php echo $User->name; ?>" placeholder="Enter Name" class="form-control">
                        </div>
                        <div class="form-group">       
                          <label class="form-control-label">Email</label>
                          <input type="email" name="email" value="<?php echo $User->email; ?>" placeholder="Enter Email" class="form-control">
                          <input type="hidden" name="hemail" value="<?php echo $User->email; ?>" >
                        </div>
						 <div class="form-group">       
                          <label class="form-control-label">Phone</label>
                          <input type="text" name="phone" value="<?php echo $User->phone; ?>" placeholder="Enter Phone" class="form-control">
                          
                        </div>
						 <div class="form-group">       
                          <label class="form-control-label">Address</label>
                          <input type="text" name="address" value="<?php echo $User->address; ?>" placeholder="Enter Address" class="form-control">
                        </div>
						 <div class="form-group">       
                          <label class="form-control-label">Dob</label>
                          <input  type="text" name="dob" value="<?php echo $User->dob; ?>" placeholder="Enter Dob" class="form-control datepicker">
                        </div>
						 <div class="form-group">       
                          <label class="form-control-label">Gender</label>
                          <select name="gender" class="form-control">
                              <option <?php if($User->gender == "Male") echo "selected='selected'"; ?>>Male</option>
                              <option <?php if($User->gender == "Female") echo "selected='selected'"; ?>>Female</option>
                              
                            </select>
                        </div>
						 <div class="form-group">       
                          <label class="form-control-label">Status</label>
                          <select name="status" class="form-control">
                              <option value="">Select</option>
                              <option  <?php if($User->status == "Active") echo "selected='selected'"; ?>>Active</option>
                              <option <?php if($User->status == "Inactive") echo "selected='selected'"; ?>>Inactive</option>
                              
                            </select>
                        </div>
                        <div class="form-group">       
                          <input type="submit" value="<?php if($User->id =='') echo 'Submit'; else echo 'Update'?>" class="btn btn-primary">
                        </div>
                      </form>
                   
				  
                  </div>
             </div>
         </div>
     </div>
 </section> <!-- /.content -->

 