 <div class="modal fade" id="change_pwd" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
       <form action="#" method="post"  id="change-password">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Password</h4>
        </div>
        <div class="modal-body">
      <div class="alert alert-danger hidden" id="alert-msg-c">
        
      </div>
          <div class="form-group">
            <label for="emailAddress">Old Password</label>            
            <input type="password" id="old-pwd" class="form-control" name="oldpassword" required> 
          </div>
      <div class="form-group">
            <label for="emailAddress">New Password</label>            
            <input type="password" id="new-pwd" class="form-control" name="newpassword" required> 
          </div>
         <div class="form-group">
            <label for="emailAddress">Confirm Password</label>            
            <input type="password" id="conf-pwd" class="form-control" name="confirmpassword" required> 
          </div>
         
        </div>
        <div class="modal-footer">
          <button type="submit" id="passwordchange" name="passwordchange" class="notes-btn btn pull-left btn-primary" >Update Password</button>
        </div>
        </form>
      </div>
    </div>
  </div> 

 