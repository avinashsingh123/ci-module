<section class="content">
        <div class="row">
            <!-- Form controls -->
            <div class="col-sm-12">
                <div class="panel panel-bd lobidisable">
                    <div class="panel-heading">
                        <div class="btn-group"> 
                          <a class="btn btn-primary btn-list" href="<?php echo base_url().'index.php/user/add_user/'?>">
                           <i class="fa fa-plus" aria-hidden="true"></i>
                          Add User</a>
                          
                      </div>
                  </div>
                  <div class="panel-body">
                      <table class="table table-striped responsive table-sm data-table">
                        <thead>
                          <tr>
                            <th>#</th>                           
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Dob</th>
                            <th>Gender</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
						<?php $i =1;
						if(!empty($User)){
							foreach($User as $dd){?>
                          <tr>
                           
                            <td><?php echo $dd->id; ?></td>
                            <td><?php echo $dd->name; ?></td>
                            <td><?php echo $dd->email; ?></td>
                            <td><?php echo $dd->phone; ?></td>
                            <td><?php echo $dd->dob; ?></td>
                            <td><?php echo $dd->gender; ?></td>
							 <td>
							<span class="label <?php if($dd->status == 'Active') echo 'label-success label-default'; else echo 'label-default label-danger';?>"><?php echo $dd->status; ?></span>
							</td>
                            <td>
								<a data-toggle="tooltip" data-placement="left" title="" data-original-title="Update" href="<?php echo base_url().'index.php/user/add_user/'.$dd->id; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
								<a data-toggle="tooltip" data-placement="left" title="" data-original-title="Delete" href="<?php echo base_url().'index.php/user/delete_user/'.$dd->id; ?>" class="btn btn-danger btn-xs Confirm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
							</td>
                          </tr>
						<?php $i++;
						}} ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
		
		 </div>
     </div>
 </section> <!-- /.content -->
         