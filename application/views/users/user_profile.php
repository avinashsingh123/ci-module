<!-- Main content -->
<section class="content">
	<div class="panel panel-info view-prof-screen">
            <div class="panel-heading">
              <h3 class="panel-title"><?php echo $details->name;?></h3>
            </div>
            <div class="panel-body">
              <div class="row">
			  <?php //echo "<pre>";
			 // print_r($details);
			 // echo "</pre>";?>
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="" src="" class="img-circle img-responsive"> </div>
                
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                      
                      <tr>
                                <td>Name</td>
                                <td><?php echo $details->name;?></td>
                              </tr>
        					  <tr>
                        <td>Date of Birth</td>
                        <td><?php echo $details->dob;?></td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td>Gender</td>
                        <td><?php echo $details->gender;?></td>
                      </tr>
                        <tr>
                        <td>Home Address</td>
                        <td><?php echo $details->address;?></td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:<?php echo $details->email;?>"><?php echo $details->email;?></a></td>
                      </tr>
                        <td>Phone Number</td>
                        <td><?php echo $details->phone;?>
                        </td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                  
                  <a href="#" class="btn btn-primary edit-profile-btn">Edit Profile</a>
                  <a href="" class="btn btn-primary" data-toggle="modal" data-target="#change_pwd">Change Password</a>
                
                </div>
              </div>
            </div>
                 <!--div class="panel-footer">
                        <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                        <span class="pull-right">
                            <a href="edit.html" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                        </span>
                    </div-->
            
          </div>
     
<div class="panel panel-info edit-prof-screen hidden">
            <div class="panel-heading">
              <h3 class="panel-title">Edit Your Details</h3>
            </div>
            <div class="panel-body">
              <div class="row">
			  <?php //echo "<pre>";
			 // print_r($details);
			 // echo "</pre>";?>
                <div class="col-md-3 col-lg-3 " align="center"> 
				<img alt="" src="#" class="img-circle img-responsive"> </div>
                
                <div class=" col-md-9 col-lg-9 "> 
				<form action="" method="post" id="update_profile">
                  <table class="table table-user-information">
                    <tbody>
                      
                    
					  <tr>
                        <td>Full Name</td>
                        <td><input type="text" name="name"  value="<?php echo $details->name;?>"/></td>
                      </tr>
					  <tr>
                        <td>Date of Birth</td>
                        <td><input type="text" name="dob" class="datepicker" value="<?php echo $details->dob;?>"/></td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td>Gender</td>
                        <td><select name="gender">
							<option value="Male" <?php if($details->gender=="Male") echo 'selected';?>>Male</option>
							<option value="Female" <?php if($details->gender=="Female") echo 'selected';?>>Female</option>
							</select>
						</td>
                      </tr>
                        <tr>
                        <td>Home Address</td>
                        <td><textarea name="address">
						<?php echo $details->address;?></textarea></td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:<?php echo $details->email;?>"><?php echo $details->email;?></a></td>
                      </tr>
                        <td>Phone Number</td>
                        <td><input type="text" name="phone" value="<?php echo $details->phone;?>"/>
                        </td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                  <button type="button" class="btn btn-primary cancel-profile-btn">Cancel</button>
                  <button class="btn btn-primary" type="submit" name="submit">Update Details</button>
				  </form>
                  
                 
                </div>
              </div>
            </div>
          </div>
     	 
</section>
		<script data-cfasync="false">
		$(window).load(function(){
			$('.edit-profile-btn,.cancel-profile-btn').click(function(){
				$('.edit-prof-screen').toggleClass('hidden');
				$('.view-prof-screen').toggleClass('hidden');
			})
		})
    //User Details Save
$('#update_profile').submit(function(){
  
   $.ajax({
                url: "<?php echo base_url().'index.php/user/update_user_profile';?>",
                dataType: 'json',
                type: 'post',
              
                data: $(this).serialize(),
                success: function( data, textStatus, jQxhr ){
                   window.location.href = data.url;
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            }); 
    return false;
});

$(document).on('submit','#change-password',function(){
    var odp = $('#old-pwd').val();
    var newp = $('#new-pwd').val();
    var conp = $('#conf-pwd').val();
    var msg = '';
    if(newp < 8){
      msg = 'Password Must be Min. 8 character';
    }
    if(newp != conp){
      msg += '<br>Password and Confirm Password Not Match';
      
    }
    if(msg != ''){
      $('#alert-msg-c').html(msg);
      $('#alert-msg-c').removeClass('hidden');
    } else{
      $('#alert-msg-c').addClass('hidden');
     $.ajax({
                  url: "<?php echo base_url().'index.php/user/update_password'?>",
                  dataType: 'json',
                  type: 'post',
                  data: $(this).serialize(),
                  success: function( data, textStatus, jQxhr ){
                       if(data.error == true){
                         //alert('errrr');
                         $('#alert-msg-c').html(data.msg);
                        $('#alert-msg-c').removeClass('hidden');
                        $('#old-pwd').val('');
                        $('#new-pwd').val('');
                        $('#conf-pwd').val('');
                         $('#old-pwd').focus();
                           } else if(data.error == false){
                             //alert("successfully Updated")
                            window.location.href = data.url;
                           } else{
                             alert("not match");
                           }
                  },
                  error: function( jqXhr, textStatus, errorThrown ){
                      $('#alert-msg-c').html('Error In Updation');
                      $('#alert-msg-c').removeClass('hidden');
                  }
              });
    }     
      return false;
  });

</script>		
				 
				 
				