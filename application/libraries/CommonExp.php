<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 Library Exception messages
*/
class CommonExp {
	
	 
    public function __construct()
    {
      
    }
	
	public function exceptions(){
	$m['add_student'] = 'Student has been added succssfully';
	$m['update_student'] = 'Student has been Updated succssfully';
	$m['empty_student_name'] = 'student name can\'t be empty.';
	$m['empty_student_email'] = 'student email can\'t be empty.';
	$m['empty_student_phone'] = 'student phone can\'t be empty.';
	$m['empty_student_gender'] = 'student gender can\'t be empty.';
	$m['email_exists'] = 'student email already exists!.';
	$m['phone_exists'] = 'student phone number already exists!.';
	$m['please_fill_field'] = 'Please fill field';
	$m['add_faculty'] = 'Faculty has been added succssfully';
	$m['update_faculty'] = 'Faculty has been Updated succssfully';
	$m['email_faculty_exists'] = 'Faculty email already exists!.';
	$m['phone_faculty_exists'] = 'Faculty phone number already exists!.';
	$m['student_login_success'] = 'Student Logged In Success.';
	$m['faculty_login_success'] = 'Faculty Logged In Success.';
	$m['admin_login_success'] = 'Admin Logged In Success.';
	$m['invalid_login'] = 'Logged In Failed.';
	$m['email_required'] = 'Email can\'t be empty.';
	$m['password_required'] = 'Password can\'t be empty.';
	$m['type_required'] = 'Please enter login type';
	$m['invalid_type'] = 'Invalid Type';
	$m['pwd_changed'] = 'Password Has been changed successfully';
	$m['error_update'] = 'Error In updation';
	$m['invalid_access'] = 'Not Accessible';
	$m['usertype_required'] = 'User Type Required';
	$m['not_verified'] = 'Admin Not Verified';
	$m['type'] = 'Data Type Required';
	$m['list_data'] = 'Data Listed success';
	$m['id_required'] = 'Id Required for Updation';
	$m['error_s'] = 'Error Occured';
	$m['del_success'] = 'Deleted Successfully';
	$m['field_name'] = 'Field Name Required';
	$m['field_name_v'] = 'Field Name Not valid';
	$m['field_value'] = 'Field Value Required';
	$m['success_upload'] = 'Uploaded Successfully';
	
	 
	return $m;
	}

}
?> 